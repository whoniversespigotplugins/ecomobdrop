package eu.whoniverse.spigot.ecomobdrop;

import eu.whoniverse.spigot.ecomobdrop.commands.ReloadCommand;
import eu.whoniverse.spigot.ecomobdrop.events.MobsSpawnEvent;
import eu.whoniverse.spigot.ecomobdrop.events.PlayerKillMobEvent;
import eu.whoniverse.spigot.ecomobdrop.utils.ResourceManager;
import lombok.Getter;
import net.milkbowl.vault.economy.Economy;
import org.bukkit.Bukkit;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.event.Listener;
import org.bukkit.plugin.RegisteredServiceProvider;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Objects;
import java.util.logging.Level;

public final class EcoMobDrop extends JavaPlugin {
    @Getter
    private static Economy economy;
    private static final HashMap<String, Object> config = new HashMap<>();

    static {
        config.put("lang", "en");
    }

    @Getter
    private final HashMap<String, String> langs = new HashMap<>();
    private final FileConfiguration configuration = this.getConfig();

    private static boolean setupEconomy() {
        RegisteredServiceProvider<Economy> economyProvider = Bukkit.getServicesManager().getRegistration(Economy.class);
        if (economyProvider != null) {
            economy = economyProvider.getProvider();
        }
        return economyProvider != null;
    }

    @Override
    public void onEnable() {
        if (!setupEconomy()) {
            getLogger().log(Level.WARNING, "No economy system detected, the server will shutdown.");
            Bukkit.shutdown();
        }
        /*
         * Component Register
         */
        Listener[] listeners = {new PlayerKillMobEvent(), new MobsSpawnEvent()};
        Arrays.asList(listeners).forEach(item -> getServer().getPluginManager().registerEvents(item, this));
        Objects.requireNonNull(getCommand("emdreload")).setExecutor(new ReloadCommand());
        /*
         * Configs creation and loading
         */
        this.createAndSaveDefConfig();

        ResourceManager.getInstance().getResourceFiles("lang").forEach(item -> {
            langs.put(item.replace("lang/", "").replace(".yml", ""), item);
            if (this.getResource(item) != null) {
                this.saveResource(item, false);
            }else{
                getLogger().log(Level.INFO,item.concat(" not create, because already exists."));
            }
        });
        ResourceManager.getInstance().loadLangFile(configuration.getString("lang"));
        ResourceManager.getInstance().reloadConfiguration();
    }

    private void createAndSaveDefConfig() {
        configuration.addDefaults(config);
        configuration.options().copyDefaults(true);
        saveConfig();
    }
}