package eu.whoniverse.spigot.ecomobdrop.models;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.bukkit.entity.EntityType;

import java.util.ArrayList;
import java.util.Objects;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class EntityConfiguration {
    private EntityType entityType;
    private Integer minAmount = 0;
    private Integer maxAmount = 0;
    private ArrayList<VipPerms> vipPerms = new ArrayList<>();
    private Boolean enable = true;
    private Boolean needsPerms = false;
    private String permNeeded = "";
    private Integer spawnerDivider = 0;

    public EntityConfiguration(EntityType entityType) {
        this.entityType = entityType;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        EntityConfiguration that = (EntityConfiguration) o;
        return entityType == that.entityType;
    }

    @Override
    public int hashCode() {
        return Objects.hash(entityType);
    }
}
