package eu.whoniverse.spigot.ecomobdrop.models;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class VipPerms {
    private String permissionString;
    private Integer multiplier;
}
