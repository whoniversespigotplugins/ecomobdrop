package eu.whoniverse.spigot.ecomobdrop.models;

import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;

@Getter
@Setter
public class Configuration {
    private ArrayList<EntityConfiguration> entitiesConfig = new ArrayList<>();
}
