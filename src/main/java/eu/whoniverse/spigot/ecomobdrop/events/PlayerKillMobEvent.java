package eu.whoniverse.spigot.ecomobdrop.events;

import eu.whoniverse.spigot.ecomobdrop.EcoMobDrop;
import eu.whoniverse.spigot.ecomobdrop.models.Configuration;
import eu.whoniverse.spigot.ecomobdrop.models.EntityConfiguration;
import eu.whoniverse.spigot.ecomobdrop.models.VipPerms;
import eu.whoniverse.spigot.ecomobdrop.utils.Misc;
import eu.whoniverse.spigot.ecomobdrop.utils.ResourceManager;
import eu.whoniverse.spigot.ecomobdrop.wrapper.LivingEntityWrapper;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.OfflinePlayer;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDeathEvent;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Optional;
import java.util.concurrent.ThreadLocalRandom;


public class PlayerKillMobEvent implements Listener {
    private net.milkbowl.vault.economy.Economy economy;
    private ResourceManager resourceManager;

    public PlayerKillMobEvent() {
        economy = EcoMobDrop.getEconomy();
        resourceManager = ResourceManager.getInstance();
    }

    @EventHandler
    public void onPlayerKillMobDropMoney(EntityDeathEvent entityDeathEvent) {
        Player killer = entityDeathEvent.getEntity().getKiller();
        LivingEntity livingEntity = entityDeathEvent.getEntity();
        LivingEntityWrapper livingEntityWrapper = new LivingEntityWrapper(livingEntity);
        Integer multiplier = 1;
        Optional<EntityConfiguration> entityConfiguration = Misc.isEntityConfigured(livingEntity.getType());
        if (killer != null && entityConfiguration.isPresent() && isThePlayerEligible(entityConfiguration.get(), killer)) {
            Integer maxAmount = entityConfiguration.get().getMaxAmount();
            Integer minAmount = entityConfiguration.get().getMinAmount();
            Optional<VipPerms> vipPerms = isVip(killer, livingEntity);
            if (vipPerms.isPresent() && vipPerms.get().getMultiplier() != 0) {
                multiplier = vipPerms.get().getMultiplier();
            }
            BigDecimal money;
            money = BigDecimal.valueOf(ThreadLocalRandom.current().nextInt(minAmount, maxAmount + 1)).multiply(BigDecimal.valueOf(multiplier));
            if (livingEntityWrapper.isSpawnedFromSpawner() && entityConfiguration.get().getSpawnerDivider() != 0) {
                money = money.divide(BigDecimal.valueOf(entityConfiguration.get().getSpawnerDivider()), RoundingMode.DOWN);
            }
            OfflinePlayer offlinePlayer = Bukkit.getOfflinePlayer(killer.getUniqueId());
            if (!economy.hasAccount(offlinePlayer)) {
                economy.createPlayerAccount(offlinePlayer);
            }
            economy.depositPlayer(offlinePlayer, money.doubleValue());
            double balance = economy.getBalance(Bukkit.getOfflinePlayer(killer.getUniqueId()));
            String msg = resourceManager.getLang().getString("rewardMessage");
            killer.sendMessage(
                    ChatColor.translateAlternateColorCodes('&', msg
                            .replace("%earned_amount%", String.valueOf(money))
                            .replace("%player_balance%", String.valueOf(balance))
                    )
            );
        }
    }

    private boolean isThePlayerEligible(EntityConfiguration entityConfiguration, Player k) {
        return (!entityConfiguration.getNeedsPerms()) || (k.hasPermission(entityConfiguration.getPermNeeded()));
    }

    private Optional<VipPerms> isVip(Player player, LivingEntity livingEntity) {
        EntityConfiguration entityConfiguration = new EntityConfiguration();
        ArrayList<VipPerms> vipPermsList;
        entityConfiguration.setEntityType(livingEntity.getType());
        Configuration configuration = resourceManager.getConfiguration();
        if (null != configuration.getEntitiesConfig() && !configuration.getEntitiesConfig().isEmpty()) {
            vipPermsList = configuration.getEntitiesConfig().get(configuration.getEntitiesConfig().indexOf(entityConfiguration)).getVipPerms();
            if (vipPermsList != null && !vipPermsList.isEmpty()) {
                for (VipPerms vipPerms : vipPermsList) {
                    if (player.hasPermission(vipPerms.getPermissionString())) {
                        return Optional.of(vipPerms);
                    }
                }
            }
        }
        return Optional.empty();
    }


}
