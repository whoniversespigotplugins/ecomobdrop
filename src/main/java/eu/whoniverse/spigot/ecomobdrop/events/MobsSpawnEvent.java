package eu.whoniverse.spigot.ecomobdrop.events;

import eu.whoniverse.spigot.ecomobdrop.EcoMobDrop;
import eu.whoniverse.spigot.ecomobdrop.models.EntityConfiguration;
import eu.whoniverse.spigot.ecomobdrop.utils.Misc;
import eu.whoniverse.spigot.ecomobdrop.utils.References;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.SpawnerSpawnEvent;
import org.bukkit.metadata.FixedMetadataValue;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.Optional;

public class MobsSpawnEvent implements Listener {
    @EventHandler
    public void onMobSpawn(SpawnerSpawnEvent spawnerSpawnEvent) {
        Optional<EntityConfiguration> entityConfigurationOptional = Misc.isEntityConfigured(spawnerSpawnEvent.getEntity().getType());
        if (entityConfigurationOptional.isPresent()) {
            EntityConfiguration entityConfiguration = entityConfigurationOptional.get();
            if (entityConfiguration.getSpawnerDivider() != null && entityConfiguration.getSpawnerDivider() != 0) {
                spawnerSpawnEvent.getEntity().setMetadata(References.CUSTOM_META, new FixedMetadataValue(JavaPlugin.getPlugin(EcoMobDrop.class), true));
            }

        }
    }
}
