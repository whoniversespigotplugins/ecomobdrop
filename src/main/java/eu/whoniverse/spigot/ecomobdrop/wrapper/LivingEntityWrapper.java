package eu.whoniverse.spigot.ecomobdrop.wrapper;

import eu.whoniverse.spigot.ecomobdrop.utils.References;
import lombok.Getter;
import org.bukkit.entity.LivingEntity;
import org.bukkit.metadata.MetadataValue;

import java.util.List;

@Getter
public class LivingEntityWrapper {
    private final LivingEntity livingEntity;

    public LivingEntityWrapper(LivingEntity livingEntity) {
        this.livingEntity = livingEntity;
    }

    public boolean isSpawnedFromSpawner() {
        List<MetadataValue> metadataValueList = this.livingEntity.getMetadata(References.CUSTOM_META);
        return !metadataValueList.isEmpty();
    }
}
