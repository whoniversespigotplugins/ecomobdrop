package eu.whoniverse.spigot.ecomobdrop.commands;

import eu.whoniverse.spigot.ecomobdrop.EcoMobDrop;
import eu.whoniverse.spigot.ecomobdrop.utils.ResourceManager;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.plugin.java.JavaPlugin;

public class ReloadCommand implements CommandExecutor {
    public ReloadCommand() {
        // No need to implements a custom constructor
    }


    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        ResourceManager.getInstance().reloadConfiguration();
        JavaPlugin.getPlugin(EcoMobDrop.class).reloadConfig();
        ResourceManager.getInstance().loadLangFile(JavaPlugin.getPlugin(EcoMobDrop.class).getConfig().getString("lang"));
        sender.sendMessage(ChatColor.GREEN + "Config reloaded!");
        return true;
    }
}
