package eu.whoniverse.spigot.ecomobdrop.utils;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import eu.whoniverse.spigot.ecomobdrop.EcoMobDrop;
import eu.whoniverse.spigot.ecomobdrop.models.Configuration;
import lombok.Getter;
import org.bukkit.configuration.InvalidConfigurationException;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URL;
import java.nio.file.Files;
import java.security.CodeSource;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.logging.Level;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

public class ResourceManager {
    private static ResourceManager instance;
    private final String dataFolder;
    private final EcoMobDrop ecoMobDrop;
    private final Gson gson;
    @Getter
    private YamlConfiguration lang = new YamlConfiguration();
    @Getter
    private Configuration configuration;

    private ResourceManager() {
        this.ecoMobDrop = JavaPlugin.getPlugin(EcoMobDrop.class);
        this.dataFolder = ecoMobDrop.getDataFolder().toPath().toString().concat(File.separator);
        this.gson = new GsonBuilder().setPrettyPrinting().create();
    }

    public static ResourceManager getInstance() {
        if (instance == null) {
            instance = new ResourceManager();
        }
        return instance;
    }

    public <T> void saveConfig(T config, String relativePath) {
        String path = dataFolder.concat(relativePath);
        try (FileOutputStream outputStream = new FileOutputStream(path)) {
            outputStream.write(gson.toJson(config).getBytes());
        } catch (IOException e) {
            ecoMobDrop.getLogger().log(Level.SEVERE, e.getMessage());
        }
    }

    public <T> Optional<T> loadFile(Class<T> tClass, String relativePath) {
        String path = dataFolder.concat(relativePath);
        try {
            return Optional.of(gson.fromJson(new String(Files.readAllBytes(new File(path).toPath())), tClass));
        } catch (IOException e) {
            ecoMobDrop.getLogger().log(Level.SEVERE, e.getMessage());
        }
        return Optional.empty();
    }

    public List<String> getResourceFiles(String path) {
        List<String> resourceList = new ArrayList<>();
        CodeSource src = this.getClass().getProtectionDomain().getCodeSource();
        URL jar = src.getLocation();
        try (ZipInputStream zipInputStream = new ZipInputStream(jar.openStream())) {
            ZipEntry entry;
            while ((entry = zipInputStream.getNextEntry()) != null) {
                String name = entry.getName();
                if (!entry.isDirectory() && !name.equalsIgnoreCase(path) && name.contains(path)) {
                    resourceList.add(name);
                }
            }
        } catch (IOException ex) {
            ecoMobDrop.getLogger().log(Level.SEVERE, ex.getMessage());
        }
        return resourceList;
    }

    public void loadLangFile(String langCode) {
        try {
            String path = ecoMobDrop.getLangs().get(langCode);
            this.lang.load(new File(dataFolder.concat("/").concat(path)));
        } catch (IOException | InvalidConfigurationException ex) {
            ecoMobDrop.getLogger().log(Level.SEVERE, ex.getMessage());
        }
    }

    public void reloadConfiguration() {
        ResourceManager
                .getInstance()
                .loadFile(Configuration.class, Configuration.class.getSimpleName().concat(".json"))
                .ifPresent(config -> this.configuration = config);
    }

}
