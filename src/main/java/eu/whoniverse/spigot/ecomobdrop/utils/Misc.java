package eu.whoniverse.spigot.ecomobdrop.utils;

import eu.whoniverse.spigot.ecomobdrop.models.Configuration;
import eu.whoniverse.spigot.ecomobdrop.models.EntityConfiguration;
import org.bukkit.entity.EntityType;

import java.util.Optional;

public class Misc {
    private Misc() {
        //No needs for implementation
    }

    public static Optional<EntityConfiguration> isEntityConfigured(EntityType entityType) {
        EntityConfiguration entityConfiguration = new EntityConfiguration(entityType);
        Configuration configuration = ResourceManager.getInstance().getConfiguration();
        if (configuration != null && !configuration.getEntitiesConfig().isEmpty()) {
            int id = configuration.getEntitiesConfig().indexOf(entityConfiguration);
            if (id > -1) {
                entityConfiguration = configuration.getEntitiesConfig().get(id);
                if (entityConfiguration != null && entityConfiguration.getEnable()) {
                    return Optional.of(entityConfiguration);
                }
            }
        }
        return Optional.empty();
    }

}
