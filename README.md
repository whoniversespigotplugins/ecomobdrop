# EcoMobDrop
> Just kill a mob , and get rewarded. Easy! 

[![Java Version](https://img.shields.io/badge/Java%20Version-1.8-<COLOR>.svg?style=flat-square)](#)
[![Maven Version](https://img.shields.io/badge/Maven%20Version-3.0.6-<COLOR>.svg?style=flat-square)](#)
[![Build Status](http://gh2.whoniverse.eu:8080/buildStatus/icon?job=EcoMobDrop&style=flat-square)](http://gh2.whoniverse.eu:8080/job/EcoMobDrop/)
[![Latest Build](https://img.shields.io/badge/Latest%20Build-Download-blue.svg?style=flat-square)](http://185.25.206.238:8080/job/EcoMobDrop/lastSuccessfulBuild/artifact/target/)
[![License](https://img.shields.io/badge/License-GPLv3-red.svg?style=flat-square)](LICENSE)
<!--[![Quality Gate Status](http://185.229.236.192:49161/api/project_badges/measure?project=eu.whoniverse.spigot%3AEcoMobDrop&metric=alert_status)](http://185.229.236.192:49161/dashboard?id=eu.whoniverse.spigot%3AEcoMobDrop)-->
## Dependencies 
* [Vault](https://www.spigotmc.org/resources/vault.34315/) (You need an economy system too!)
 
## Todo List
* Mob head drop
* New Enchantment (Something alike Looting)
## Configuration

Create a Configuration.json file inside the EcoMobDrop folder.

The configuration file of this plugin use a JSON Syntax, so it's pretty easy to configure.
##### Some examples:
That's an array called entitiesConfig (Empty)
```sh
{
  "entitiesConfig": [
  ]
}
```
That's a complex object with some properties. You can check all entityType [here](https://hub.spigotmc.org/javadocs/spigot/org/bukkit/entity/EntityType.html)
```    
{
   "entityType":"BAT",
   "minAmount":1,
   "maxAmount":1,
   "vipPerms":[
      {
         "permissionString":"*",
         "multiplier":10
      }
   ],
   "enable":true
}
```
Now let's put the object inside the array (every array item MUST be separated by a comma)
```
{
   "entitiesConfig":[
       {
          "entityType": "ZOMBIE",
          "minAmount": 100,
          "maxAmount": 1000,
          "vipPerms": [
             {
                "permissionString": "vip.zombie.reward",
                "multiplier": 2
             }
          ],
          "enable": true,
          "needsPerms": true,
          "permNeeded": "zombie.reward",
          "spawnerDivider": 10
       },
      {
         "entityType":"BAT",
         "minAmount":1,
         "maxAmount":1,
         "spawnerDivider": 200,
         "vipPerms":[
            {
               "permissionString":"*",
               "multiplier":10
            }
         ],
         "enable":true
      }
   ]
}
```

## Powered by Intellij
<div align="center">
   <a href="https://www.jetbrains.com"><img src="jetbrains.png" width="75"></a>
</div>